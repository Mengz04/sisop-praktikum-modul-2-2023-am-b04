#include <unistd.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <signal.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/types.h>

int main(int argc, char* argv[]){
	if (argc == 1) exit(1);
	if (!strcmp("-a", argv[1])) {
		FILE *killer_file = fopen("killer.c", "w+");

	    fprintf(killer_file, "#include<stdio.h>\n"
							  "#include<stdlib.h>\n"
							  "#include<unistd.h>\n"
							  "int main() {\n");
		fprintf(killer_file, "int pid = fork();\n if(pid ) execl(\"/usr/bin/pkill\", \"pkill\", \"-f\", \"download_process\", NULL);\n");
		fprintf(killer_file, "execl(\"/usr/bin/pkill\", \"pkill\", \"-f\", \"create_process\", NULL);\n return 1;}\n");
		fclose(killer_file);
		int status = 0;
	    pid_t kpid = fork();
	    if(!kpid) {
			char *gccKill1[] = {"gcc", "killer.c", "-o", "killer", NULL};
			execv("/usr/bin/gcc", gccKill1);
		}
	    waitpid(kpid, &status, 0);
	    kpid = fork();
	    if(!kpid){
			char *rmKill1[] = {"rm", "killer.c", NULL};
			execv("/usr/bin/rm", rmKill1);
		}
    }
	else if (!strcmp("-b", argv[1])) {
		FILE *killer_file = fopen("killer.c", "w+");

	    fprintf(killer_file, "#include<stdio.h>\n"
							  "#include<stdlib.h>\n"
							  "#include<unistd.h>\n"
							  "int main() {\n");
		fprintf(killer_file, "execl(\"/usr/bin/pkill\", \"pkill\", \"-f\", \"create_folder\", NULL);\n return 1;}\n");
		fclose(killer_file);
		int status = 0;
	    pid_t kpid = fork();
	    if(!kpid){
			char *gccKill2[] = {"gcc", "killer.c", "-o", "killer", NULL};
			execv("/usr/bin/gcc", gccKill2);
		}
	    waitpid(kpid, &status, 0);

	    kpid = fork();
	    if(!kpid){
			char *rmKill2[] = {"rm", "killer.c", NULL};
			execv("/usr/bin/rm", rmKill2);
		}
    }
	else{
		exit(0);
	}
	
	char fld_name[50], img_name[50], img_path[120];
	int fld_pid, img_pid;
	while(1) {
		fld_pid = fork();
		sprintf(argv[0], "create_process");
		if (fld_pid) { 
			sleep(30);
			continue;
		}
		time_t time_folder;
		time(&time_folder);
		strftime(fld_name, 100, "%Y-%m-%d_%H:%M:%S", localtime(&time_folder));
		mkdir(fld_name, 0777);
		for(int i = 0; i < 15; i++) {
			img_pid = fork();
			sprintf(argv[0], "download_process");
			if(img_pid) { 
				sleep(5);
				continue;
			}
			time_t time_image;
			time(&time_image);

			strftime(img_name, 100, "%Y-%m-%d_%H:%M:%S.jpg", localtime(&time_image));
			sprintf(img_path, "%s/%s", fld_name, img_name);

			char img_url[100];
			sprintf(img_url, "https://picsum.photos/%ld", ((unsigned long)time(NULL))%1000+50);

			char *wgetImg[] = {"wget", "-q", "-O", img_path, img_url, NULL};
			execv("/usr/bin/wget", wgetImg);
            exit(1);
		}
		int zstatus = 0;
		int zpid = fork();
		if(!zpid){
			char *zipFld[] = {"zip", "-r", fld_name, fld_name, NULL};
			execv("/usr/bin/zip", zipFld);
		} 
		waitpid(zpid, &zstatus, 0); 
		char *rmFld[] = {"rm", "-rf", fld_name, NULL};
		execv("/usr/bin/rm", rmFld);
		exit(1);
	}
	return 1;
}

