# sisop-praktikum-modul-2-2023-AM-B04

## Anggota Kelompok B04:
+ Akbar Putra Asenti Priyanto (5025211004)
+ Muhammad Rafi Sutrisno (5025211167)
+ Muhammad Rafi Insan Fillah (5025211169)



## Soal 1
# binatang.c

```
int main(){
	pid_t pid = fork();
	int status;
	if(pid == 0){
		//download zip
                char *download[]={"wget", "-O", "coba.zip","https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq",NULL};
                execv("/usr/bin/wget", download);

	}else{

```
Pertama buat fork terlebih dahulu untuk menjalankan 2 process, lalu download menggunakan wget dan execv pada child process, disini file downloadnya saya namakan coba.zip.
```
//tunggu selesai zip
		while ((wait(&status)) > 0);
		pid_t pidm = fork();
		int statusm;
		if(pidm==0){
			//buat file untuk simpan gambar
			char *mkdi[]={"mkdir", "file", NULL};
			execv("/usr/bin/mkdir", mkdi);
		}

```
Lalu untuk parent process nya menggunakan wait untuk menunggu child process selesai lalu membuat folder bernama file menggunakan mkdir dan execv. Lalu fork lagi agar bisa menjalankan process lain.
```
            pid_t pid2 = fork();
			int status2;
			char path[1024];
   			getcwd(path, sizeof(path));
    		strcat(path, "/file");
			if(pid2==0){
				//unzip ke file
                char *args[] = {"unzip", "coba.zip","-d", path,  NULL};
        	    execv("/usr/bin/unzip", args);
            }
```
Process selanjutnya adalah pertama ambil path dari folder bernama file yang telah dibuat tadi lalu unzip file yang didownload tadi menggunakan perintah unzip dan execv. Lalu fork lagi untuk menjalankan process lain.
```
else{
				//tunggu selesai unzip
				while ((wait(&status2)) > 0);
				pid_t pid3 = fork();
				if(pid3==0){
					//hapus file zip yg didownload
					char *remove[]={"rm", "coba.zip", NULL};
	                		execv("/usr/bin/rm", remove);
                }
```
Selanjutnya tunggu selesai unzip lalu hapus file zip awal yang didownload tadi menggunakan perintah rm dengan execv. Lalu fork lagi untuk menjalankan process lain.
```
else{
					pid_t pid4 = fork();
					if(pid4==0){
						//buat folder hewan darat, air, amphibi
						char *mkdir[]={"mkdir", "HewanDarat", "HewanAmphibi", "HewanAir", NULL};
						execv("/usr/bin/mkdir", mkdir);
                    }
```
Lalu buat 3 folder bernama HewanDarat, HewanAmphibi, dan HewanAir menggunakan  perintah mkdir dan execv.Lalu fork lagi untuk menjalankan process lain.
```
else{
						pid_t pid5 = fork();
						int status3;
						if(pid5 == 0){
							//output random
							char command[200];
        					sprintf(command, "ls %s | shuf -n 1", path);
        					system("echo hewan random :");
        					system(command);
                        }
```
Lalu ambil nama file random. Disini saya menggunakan command ls dari path folder file yang berisi file file hewan tadi lalu dijadikan input untuk dimasukkan ke perintah shuf dimana perintah tersebut akan memilih nama file random. Lalu jalankan perintah tersebut menggunakan system().
```
else{
							//tunggu ambil hewan random
							while((wait(&status3)) > 0);
							pid_t pid6 = fork();
							int status4;
							if(pid6 == 0){
								//masukin ke folder
								

								char last[200];
   								DIR *d;
  								struct dirent *dir;
							        const char *prefix1 = "_darat.jpg";
    								const char *prefix2 = "_air.jpg";
    								const char *prefix3 = "_amphibi.jpg";
    								//const char *path = "/home/vagrant/SisopSoal1/file";
    								
    								// open current directory
    								d = opendir(path);
    								if (d) {
    								    while ((dir = readdir(d)) != NULL){
    								        if (strstr(dir->d_name, prefix1) != NULL) {
    								        	char path2[1024];
   												getcwd(path2, sizeof(path2));
    											strcat(path2, "/HewanDarat");
    								            snprintf(last, sizeof(last), "mv %s/%s %s", path, dir->d_name, path2);
    								            // move file to new directory
    								            printf("%s\n\n", last);
    								            system(last);
    								        }else if(strstr(dir->d_name, prefix2) != NULL){
    								        	char path2[1024];
   												getcwd(path2, sizeof(path2));
    											strcat(path2, "/HewanAir");
    								            snprintf(last, sizeof(last), "mv %s/%s %s", path, dir->d_name, path2);
    								            // move file to new directory
    								            printf("%s\n\n", last);
    								            system(last);
   								            }else if(strstr(dir->d_name, prefix3) != NULL){
   								            	char path2[1024];
   												getcwd(path2, sizeof(path2));
    											strcat(path2, "/HewanAmphibi");
   								            	snprintf(last, sizeof(last), "mv %s/%s %s", path, dir->d_name, path2);
   								            	// move file to new directory
   								                printf("%s\n\n", last);
   								                system(last);
   								         }
   								     }
  								     closedir(d);
 								 }
								system("rmdir file");
								
							}

```
Selanjutnya pindahkan semua file yang ada di folder file ke dalam folder hewan sesuai dengan nama filenya. Disini saya menggunakan opendir ke path yang dari folder file berisi file hewan lalu membaca satu satu nama file tersebut. Selanjutnya saya cek nama file tersebut menggunaka perintah strstr dimana perintah tersebut mengecek apakah string pertama mempunyai bagian dari string kedua. Disini saya mengecek 3 kali yaitu jika nama file mengandung _Darat atau _Air atau _Amphibi lalu jika ada maka saya akan pindahkan file tersebut menggunakan perintah mv ke path folder yang sesuai dengan nama hewan tersebut. Jika sudah selesai memindahkan semua selanjutanya adalah menghapus folder file yang sekarang telah kosong.
```
else{
								while((wait(&status4)) > 0);
								//zip file
								pid_t pid7 = fork();
								if(pid7==0){
									char *zip1[]={"zip", "-r", "HewanDarat.zip", "HewanDarat", NULL};
									execv("/usr/bin/zip", zip1);
								}else{
									pid_t pid8 = fork();
									if(pid8==0){
										char *zip2[]={"zip", "-r", "HewanAir.zip", "HewanAir", NULL};
                                                                        	execv("/usr/bin/zip", zip2);
									}else{
										pid_t pid9 = fork();
										int status5;
										if(pid9==0){
											char *zip3[]={"zip", "-r", "HewanAmphibi.zip", "HewanAmphibi", NULL};
		                                                                        execv("/usr/bin/zip", zip3);
										}else{
											while((wait(&status5)) > 0);
											system("rm -r HewanDarat HewanAir HewanAmphibi");
										}
									}
								}

```
Lalu terakhir pada kode diatas buat fork untuk 3 process awal yang mengzip semua folder binatang. Lalu fork terakhir yang menunggu process sebelumnya selesai digunakan untuk meremove semua folder awal binatang tadi.

## Soal 2
# lukisan.c

```
if (argc == 1) exit(1);
```
Evaluasi apabila program dirun tanpa ada argumen program killer, maka akan exit(1). Bila argumen -a maka akan dibuat killer.c yang akan diisi dengan command berikut:
```
if (!strcmp("-a", argv[1])) {
		FILE *killer_file = fopen("killer.c", "w+");

	    fprintf(killer_file, "#include<stdio.h>\n"
							  "#include<stdlib.h>\n"
							  "#include<unistd.h>\n"
							  "int main() {\n");
		fprintf(killer_file, "int pid = fork();\n if(pid ) execl(\"/usr/bin/pkill\", \"pkill\", \"-f\", \"download_process\", NULL);\n");
		fprintf(killer_file, "execl(\"/usr/bin/pkill\", \"pkill\", \"-f\", \"create_process\", NULL);\n return 1;}\n");
		fclose(killer_file);
		int status = 0;
	    pid_t kpid = fork();
	    if(!kpid) {
			char *gccKill1[] = {"gcc", "killer.c", "-o", "killer", NULL};
			execv("/usr/bin/gcc", gccKill1);
		}
	    waitpid(kpid, &status, 0);
	    kpid = fork();
	    if(!kpid){
			char *rmKill1[] = {"rm", "killer.c", NULL};
			execv("/usr/bin/rm", rmKill1);
		}
    }
```
Killer.c tersebut akan kill proses create folder dan download gambar pada tiap foldernya. Sedangkan bila argumen -b maka akan dibuar program killer yang akan diisi dengan vommand berikut:
```
else if (!strcmp("-b", argv[1])) {
		FILE *killer_file = fopen("killer.c", "w+");

	    fprintf(killer_file, "#include<stdio.h>\n"
							  "#include<stdlib.h>\n"
							  "#include<unistd.h>\n"
							  "int main() {\n");
		fprintf(killer_file, "execl(\"/usr/bin/pkill\", \"pkill\", \"-f\", \"create_folder\", NULL);\n return 1;}\n");
		fclose(killer_file);
		int status = 0;
	    pid_t kpid = fork();
	    if(!kpid){
			char *gccKill2[] = {"gcc", "killer.c", "-o", "killer", NULL};
			execv("/usr/bin/gcc", gccKill2);
		}
	    waitpid(kpid, &status, 0);

	    kpid = fork();
	    if(!kpid){
			char *rmKill2[] = {"rm", "killer.c", NULL};
			execv("/usr/bin/rm", rmKill2);
		}
    }
```
killer.c tersebut akan kill hanya proses create folder.

Agar dapat membedakan proses create folder dan download gambar maka digunakan sprintf yang nantinya akan digunakan oleh killer file untuk kill proses dengan nama proses yang telah direname tersebut. 
```
char fld_name[50], img_name[50], img_path[120];
	int fld_pid, img_pid;
	while(1) {
		fld_pid = fork();
		sprintf(argv[0], "create_process");
		if (fld_pid) { 
			sleep(30);
			continue;
		}
		time_t time_folder;
		time(&time_folder);
		strftime(fld_name, 100, "%Y-%m-%d_%H:%M:%S", localtime(&time_folder));
		mkdir(fld_name, 0777);
		for(int i = 0; i < 15; i++) {
			img_pid = fork();
			sprintf(argv[0], "download_process");
			if(img_pid) { 
				sleep(5);
				continue;
			}
			time_t time_image;
			time(&time_image);

			strftime(img_name, 100, "%Y-%m-%d_%H:%M:%S.jpg", localtime(&time_image));
			sprintf(img_path, "%s/%s", fld_name, img_name);

			char img_url[100];
			sprintf(img_url, "https://picsum.photos/%ld", ((unsigned long)time(NULL))%1000+50);

			char *wgetImg[] = {"wget", "-q", "-O", img_path, img_url, NULL};
			execv("/usr/bin/wget", wgetImg);
            exit(1);
		}
		int zstatus = 0;
		int zpid = fork();
		if(!zpid){
			char *zipFld[] = {"zip", "-r", fld_name, fld_name, NULL};
			execv("/usr/bin/zip", zipFld);
		} 
		waitpid(zpid, &zstatus, 0); 
		char *rmFld[] = {"rm", "-rf", fld_name, NULL};
		execv("/usr/bin/rm", rmFld);
		exit(1);
	}
	return 1;
```
Selanjutnya untuk proses pembuatan folder menggunakan fork dalam while dengan sleep(30) untuk create folder setiap 30 detik, lalu mendapatkan waktu dengan time() dan strftime() untuk memberi nama folder_name dengan format waktu yang telah ditentukan, kemudian mkdir menggunakan nama folder_name dengan permission rwxrwxrwx (777). Dilanjutkan dengan proses download sebanyak 15 kali menggunakan looping for yang pada tiap iterasi loopingnya terdapat fork dan rename proses sebagai “download_image”. Child process tiap process download akan handle sleep(5) dilanjutkan dengan fetch waktu dengan time() dan strftime() untuk memberi nama image_name dengan format waktu yang telah ditentukan. Juga dilakukan sprintf() untuk path gambar yang akan didownload yang mana berada pada folder_name/image_name. Dilanjutkan dengan proses download dengan wget. Pada akhir proses tiap folder terdapat fork kembali untuk zip folder tersebut. Child process handle zip folder dilanjutkan dengan remove folder tersebut.

## Soal 3
# filter.c

Pembuatan fork pertama untuk child process download zip file dari drive link.
```
int main(){
   pid_t child_id;
   
   child_id = fork();
   if(child_id < 0){exit(EXIT_FAILURE);}
   
   if(child_id == 0){
   	char *download[] = {"wget", "-O", "players.zip", "https://drive.google.com/uc?export=download&id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF", NULL};
   	execv("/usr/bin/wget", download);
   	exit(EXIT_SUCCESS);
   }

```
Selanjutnya pada parent process fork kedua dengan child processnya fork keempat yang mana child process nya untuk unzip zip file dan parent process nya remove file zip setelah diunzip
```
else{
   	wait(NULL);
   	pid_t child_id2;
   
   	child_id2 = fork();
   	if(child_id2 < 0){exit(EXIT_FAILURE);}
   	
   	if(child_id2 == 0){
	   pid_t child_unzip;
	   child_unzip = fork();
	   if(child_unzip < 0){exit(EXIT_FAILURE);}
   	   
   	   if(child_unzip == 0){
   	   	char *unzip[] = {"unzip", "players.zip", NULL};
   	   	execv("/usr/bin/unzip", unzip);
   	   	exit(EXIT_SUCCESS);
	   }
	   else{
		wait(NULL);
		execlp("rm", "rm", "players.zip", (char *) NULL);
	   }
	   exit(EXIT_SUCCESS);
   	}

```
Parent process dari fork kedua berisi fork kelima yang child process nya untuk filter foto yang telah diunzip menggunakan find command untuk remove file yang pada kolom keduanya tidak bertuliskan “ManUtd”.
```
else{
   	   wait(NULL);

   	   pid_t child_id3;
   	   
   	   child_id3 = fork();
   	   if(child_id3 < 0){exit(EXIT_FAILURE);}
   	   
   	   if(child_id3 == 0){
   	   	char *findMU[] = {"find", "./players", "-type", "f", "!", "-name", "*_ManUtd*", "-delete", NULL};
   	   	execvp(findMU[0], findMU);
   	   	exit(EXIT_SUCCESS);
   	   }

```
Baris 60-117 merupakan serangkaian fork yang digunakan untuk membuat directory untuk masing-masing klasifikasi posisi pemain.
```
else{
   	   	wait(NULL);
   	   	pid_t child_id4;
   	   	
   	   	child_id4 = fork();
   	   	if(child_id4 < 0){exit(EXIT_FAILURE);}
   	   
   	   	if(child_id4 == 0){
   	   		wait(NULL);
	   	   	pid_t child_id5;
	   	   	
	   	   	child_id5 = fork();
	   	   	if(child_id5 < 0){exit(EXIT_FAILURE);}
	   	   
	   	   	if(child_id5 == 0){
	   	   	   pid_t child_Dir1;
	   	   	   child_Dir1 = fork();
	   	   	   
	   	   	   if(child_Dir1 < 0){exit(EXIT_FAILURE);}
	   	   	   
	   	   	   if(child_Dir1 == 0){
	   	   	   	char *GKCategory[] = {"mkdir", "-p", "./players/Kiper", NULL};
	   	   		execv("/bin/mkdir", GKCategory);
	   	   		exit(EXIT_SUCCESS);
	   	   	   }
	   	   	   else{
	   	   	   	wait(NULL);
	   	   	   	pid_t child_Dir2;
	   	   	   	child_Dir2 = fork();
	   	   	   
	   	   	   	if(child_Dir2 < 0){exit(EXIT_FAILURE);}
	   	   	   
	   	   	   	if(child_Dir2 == 0){
	   	   	   	   char *BKCategory[] = {"mkdir", "-p", "./players/Bek", NULL};
	   	   		   execv("/bin/mkdir", BKCategory);
	   	   		   exit(EXIT_SUCCESS);
	   	   	   	}
	   	   	   	else{
	   	   	   	   wait(NULL);
	   	   	   	   pid_t child_Dir3;
	   	   	   	   child_Dir3 = fork();
	   	   	   
	   	   	   	   if(child_Dir3 < 0){exit(EXIT_FAILURE);}
	   	   	   
	   	   	   	   if(child_Dir3 == 0){
	   	   	   	   	char *GLCategory[] = {"mkdir", "-p", "./players/Gelandang", NULL};
	   	   			execv("/bin/mkdir", GLCategory);
	   	   			exit(EXIT_SUCCESS);
	   	   	   	   }
	   	   	   	   else{
	   	   	   	   	wait(NULL);
	   	   	   	   	char *STCategory[] = {"mkdir", "-p", "./players/Penyerang", NULL};
	   	   			execv("/bin/mkdir", STCategory);
	   	   	   	   }
	   	   	   	}
	   	   	   }
	   	   	exit(EXIT_SUCCESS);
	   	   	}

```
Baris 118-159 merupakan serangkaian fork yang digunakan untuk mencari file dengan kata kunci posisi pemain pada kolom ketiga nama file lalu dipindahkan kedalam folder klasifikasi masing-masing.
```
else{
	   	   	  wait(NULL);
			  pid_t child_cp;
			  
			  child_cp = fork();
			  
			  if(child_cp < 0){exit(EXIT_FAILURE);}
			  
			  if(child_cp == 0){
			  	pid_t child_subcp1;
			  	child_subcp1 = fork();
			  	
			  	if(child_subcp1 < 0){exit(EXIT_FAILURE);}
			  
			  	if(child_subcp1 == 0){
			  		char *copyGK[] = {"find", "./players","-type", "f", "-name", "*_*_Kiper*", "-exec", "mv", "-n", "{}", "./players/Kiper", ";", NULL};
	   	   	   		execvp("find", copyGK);
	   	   	   		exit(EXIT_SUCCESS);
			  	}
			  	else{
			  		char *copyBK[] = {"find", "./players", "-type", "f", "-name", "*_*_Bek*", "-exec", "mv", "-n", "{}", "./players/Bek", ";", NULL};
	   	   	   		execvp("find", copyBK);
			  	}
			  }
			  else{
			  	pid_t child_subcp2;
			  	child_subcp2 = fork();
			  	
			  	if(child_subcp2 < 0){exit(EXIT_FAILURE);}
			  
			  	if(child_subcp2 == 0){
			  		char *copyGL[] = {"find", "./players", "-type", "f", "-name", "*_*_Gelandang*", "-exec", "mv", "-n", "{}", "./players/Gelandang", ";", NULL};
	   	   	   		execvp("find", copyGL);
			  	}
			  	else{
			  		char *copyST[] = {"find", "./players", "-type", "f", "-name", "*_*_Penyerang*", "-exec", "mv","-n", "{}", "./players/Penyerang", ";", NULL};
	   	   	   		execvp("find", copyST);
			  	}
			  }
	   	   	}
   	   		exit(EXIT_SUCCESS);
   	   	}

```
Parent process dari fork kelima terdapat fork yang child process nya untuk touch file txt formasi dan parent processnya memanggil function buatTim dengan parameter jumlah bek, gelandang, dan penyerang.
```
else{
 			wait(NULL);
 			pid_t child_txt;
 			
			child_txt = fork();
			  	
			if(child_txt < 0){exit(EXIT_FAILURE);}
			
			if(child_txt == 0){ //child
				char file_path[50];
				sprintf(file_path, "%s/Formasi-3-3-4.txt", getenv("HOME"));
				char *touchTxt[] = {"touch", file_path, NULL};
				execvp("touch", touchTxt);
				exit(EXIT_SUCCESS);
			}
			else{
				wait(NULL);
				buatTim(3, 3, 4);
	 		}
   	   	}

```
Function buat tim menerima argumen jumlah bek, gelandang, dan penyerang pada tim utama. Pertama dibuka file formasi yang sudah ditouch sebelumnya lalu memanggil function recursiveList dengan parameter directory klasifikasi, file txt formasi, dan jumlah pemain yang diinginkan pada posisi tersebut.
```
void buatTim(int bek, int gelandang, int penyerang){
	char file_dest[50];
	FILE *fp;
	
	sprintf(file_dest, "%s/Formasi-3-3-4.txt", getenv("HOME"));
	fp = fopen(file_dest, "w");

	recursiveList("players/Kiper", fp, 1);
	recursiveList("players/Bek", fp, bek);
	recursiveList("players/Gelandang", fp, gelandang);
	recursiveList("players/Penyerang", fp, penyerang);
}

```
Function recursiveList pada dasarnya akan mencari rating tertinggi pada sebuah klasifikasi dengan melihat kolom terakhir dari nama file pada directory klasifikasi argument menggunakan directory listing. Skor tertinggi dari setiap iterasi listing akan dicari kembali nama file yang memiliki rating tersebut dan lalu nama file tersebut diprint kedalam file txt formasi. Proses tersebut diulang sebanyak jumlah pemain pada klasifikasi tersebut yang ingin dimasukkan kedalam tim utama.
```

void recursiveList(char *basePath, FILE *fp, int num){		
	DIR *dp;
    struct dirent *rate, *max;
    
    char highestStr[3];
    char visited[num][100];
    
    int visitCount=0;
	int highest=0;

	while(num--){
		int highest=0;
		dp = opendir(basePath);
		if(dp == NULL){exit(EXIT_FAILURE);}
		while ((rate = readdir(dp))!= NULL) {
			if(!strcmp(rate->d_name, ".") || !strcmp(rate->d_name, "..")){continue;}
			int flag = 0;
			for(int i=0; i<visitCount; i++){
				if(!strcmp(visited[i], rate->d_name)){flag = 1;}
			}
			if(flag == 1){continue;}

			char *scoreStr = strrchr(rate->d_name, '_');
			int score = atoi(scoreStr+1);
			highest = (score > highest)? score : highest;
		}

		sprintf(highestStr, "%d", highest);
		dp = opendir(basePath);
		
		while ((max = readdir(dp)) != NULL) {
			if(!strcmp(max->d_name, ".") || !strcmp(max->d_name, "..")){continue;}
			if(strstr(max->d_name, highestStr) != NULL){
				fprintf(fp, "%s\n", max->d_name);
				strcpy(visited[visitCount], max->d_name);
				visitCount++;
			}
		}
	}
	(void) closedir (dp);
}

```

## Soal 4
# mainan.c

mainan.c merupakan sebuah program yang bekerja mirip seperti cronjob dimana dibutuhkan empat buah input yaitu input jam, menit, detik, dan sebuah path ke file .sh. Input waktu yang diberikan menentukan kapan file .sh di input path akan dijalankan. Diberikan juga ketentuan bahwa input waktu (jam, menit, dan detik) dapat berupa \*, yang menunjukkan bahwa program dapat dijalankan pada jam/menit/detik kapanpun. Diberi ketentuan pula bahwa program ini perlu dijalankan secara daemon process.

```
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include <time.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>
```
Berikut adalah library yang digunakan dalam kode.
```
int digits_only(const char *s){ //Mengecek apakah argumen berupa integer
    while(*s){
        if(isdigit(*s++) == 0) return 0;
    }
    return 1;
}
```
Dibutuhkan fungsi untuk mengecek integer untuk input jam, waktu, dan detik.
```
int calculatesleep(int hour, int min, int sec){ //Fungsi mengecek waktu sleep ke loop selanjutnya
    time_t now = time(NULL);
    struct tm *current_tm = localtime(&now);
    struct tm *target_tm = current_tm;
    int choice = 0;

    if(sec < 0 && min < 0 && hour < 0){ //case 1: h*m*s*
        target_tm->tm_sec = current_tm->tm_sec + 1;
        target_tm->tm_min = current_tm->tm_min;
        target_tm->tm_hour = current_tm->tm_hour;
    } else if(sec < 0 && min < 0 && hour >= 0){ //case 2: h0m*s*
        if(current_tm->tm_hour != hour){
            target_tm->tm_sec = 0;
            target_tm->tm_min = 0;
            target_tm->tm_hour = hour;
        }else{
            target_tm->tm_sec = current_tm->tm_sec + 1;
            target_tm->tm_min = current_tm->tm_min;
            target_tm->tm_hour = current_tm->tm_hour;
            if(target_tm->tm_sec >= 60){
                target_tm->tm_sec = 0;
                target_tm->tm_min + 1;
            }
            if(target_tm->tm_min >= 60){
                target_tm->tm_min = 0;
                target_tm->tm_hour + 1;
            }
        }
    } else if(sec < 0 && min >= 0 && hour < 0){ //case 3: h*m0s* 
        target_tm->tm_min = min;
        if(current_tm->tm_min < min){ 
            target_tm->tm_sec = 0;
            target_tm->tm_hour = current_tm->tm_hour;
        } else if(current_tm->tm_min == min){ 
            target_tm->tm_sec = current_tm->tm_sec + 1;
            target_tm->tm_hour = current_tm->tm_hour;
            if(target_tm->tm_sec >= 60){
                target_tm->tm_sec = 0;
                target_tm->tm_hour + 1;
            }
        } else{ 
            target_tm->tm_sec = 0;
            target_tm->tm_hour = current_tm->tm_hour + 1;
        }
    } else if(sec < 0 && min >= 0 && hour >= 0){ //case 4: h0m0s*
        if(current_tm->tm_hour != hour || current_tm->tm_min != min){
            target_tm->tm_sec = 0;
            target_tm->tm_min = min;
            target_tm->tm_hour = hour;
        }else{
            target_tm->tm_sec = current_tm->tm_sec + 1;
            target_tm->tm_min = min;
            target_tm->tm_hour = hour;
            if(target_tm->tm_sec >= 60){
                target_tm->tm_sec = 0;
                target_tm->tm_min = min;
                target_tm->tm_hour = hour;
            }
        }
    } else if(sec >= 0 && min < 0 && hour < 0){ //case 5: h*m*s0
        if(current_tm->tm_sec < sec){
            target_tm->tm_sec = sec;
            target_tm->tm_min = current_tm->tm_min;
            target_tm->tm_hour = current_tm->tm_hour;
        } else {
            current_tm->tm_sec = sec;
            target_tm->tm_min = current_tm->tm_min + 1;
            target_tm->tm_hour = current_tm->tm_hour;
            if(target_tm->tm_min >= 60){
                target_tm->tm_hour += 1;
            }
        }
    } else if(sec >= 0 && min < 0 && hour >= 0){ //case 6: h0m*s0
        if(current_tm->tm_hour != hour){
            target_tm->tm_hour = hour;
            target_tm->tm_sec = sec;
            target_tm->tm_min = 0;
        } else{
            if(current_tm->tm_sec < sec){
                target_tm->tm_hour = hour;
                target_tm->tm_min = current_tm->tm_min;
                target_tm->tm_sec = sec;
            }else if(current_tm->tm_sec == sec){
                target_tm->tm_hour = hour;
                target_tm->tm_min = current_tm->tm_min + 1;
                target_tm->tm_sec = sec;
                if(target_tm->tm_min >= 60){
                    target_tm->tm_min = 0;
                    target_tm->tm_hour = hour;
                }
            }else{
                target_tm->tm_hour = hour;
                target_tm->tm_sec = sec;
                target_tm->tm_min = current_tm->tm_min + 1;
                if(target_tm->tm_min >= 60){
                    target_tm->tm_min = 0;
                    target_tm->tm_hour = hour;
                }
            }
        }
    } else if(sec >= 0 && min >= 0 && hour < 0){ //case 7: h*m0s0
        if(current_tm->tm_min != min || current_tm->tm_sec != sec){
            target_tm->tm_sec = sec;
            target_tm->tm_min = min;
            target_tm->tm_hour = current_tm->tm_hour;
        } else{
            target_tm->tm_sec = sec;
            target_tm->tm_min = min;
            target_tm->tm_hour = current_tm->tm_hour + 1;
            if(target_tm->tm_hour >= 23){
                target_tm->tm_sec = sec;
                target_tm->tm_min = min;
                target_tm->tm_hour = 0;
                target_tm->tm_mday = current_tm->tm_mday + 1;
            }
        }
    } else{ //case 8: h0m0s0
        target_tm->tm_sec = sec;
        target_tm->tm_min = min;
        target_tm->tm_hour = hour;
    }
    time_t target = mktime(target_tm);
    double diff = difftime(target, now);

    if (diff == 0) diff++;

    if(diff < 0){
        if(sec < 0) diff++;
        if(diff < 0 && min < 0) diff += 60;
        if(diff < 0 && hour < 0) diff += 3600;
        if(diff < 0) diff += 86400;
    } 

    printf("Tidur selama %.1lf detik\n", diff);
    return diff;
}
```
Dibuat fungsi calculateSleep() untuk menghitung waktu sleep dari program hingga program dijalankan kembali. Cara kerjanya adalah dengan mengambil waktu sekarang dan target waktu yang dibutuhkan kemudian dicari perbedaan waktu antar kedua waktu tersebut. Terdapat delapan buah case karena setiap input waktu (jam, menit, atau detik) memiliki dua kemungkinan, yaitu input berupa integer atau input berupa \*. Setiap case akan melakukan perhitungan waktu yang berbeda-beda pula.
```
void startdaemon(){ //Membuat daemon process
    pid_t pid, sid;

    pid = fork();

    if(pid < 0){
        exit(EXIT_FAILURE);
    } else if(pid > 0){
        exit(EXIT_SUCCESS);
    }

    umask(0);
    sid = setsid();
    
    if(sid < 0){
        exit(EXIT_FAILURE);
    }
    
    if((chdir("/")) < 0){
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
}
```
Diperlukan fungsi startdaemon() untuk membuat proses daemon baru sesuai dengan ketentuan soal.
```
int main (int argc, char *argv[]){
    int errflag = 0;
    int hour, min, sec;
    char *path;
    char *pbash = "/bin/bash";
```
Variabel errflag untuk mengecek apabila terdapat kesalahan dalam input awal, kemudian program juga menyimpan input jam, menit, detik, dan path dari file yang akan dirun serta alamat dari bash (untuk melakukan running file path).
```
    if(argc != 5){
        printf("ERROR: Jumlah argumen tidak sesuai\n");
        return 0;
    }
```
Mengecek apakah jumlah argumen yang diinput sesuai dengan ketentuan.
```
    if(digits_only(argv[1])){ //Cek jam
        hour = atoi(argv[1]);
        if(hour < 0 || hour > 23){
            printf("ERROR: Nilai jam tidak tepat\n");
            errflag = 1;
        }
    } else if(strcmp(argv[1], "*") == 0){ //Cek "*"
        hour = -1;
    } else{
        printf("ERROR: Nilai jam tidak tepat\n");
        errflag = 1;
    }

    if(digits_only(argv[2])){ //Cek Menit
        min = atoi(argv[2]);
        if(min < 0 || min > 59){
            printf("ERROR: Nilai menit tidak tepat\n");
            errflag = 1;
        }
    } else if(strcmp(argv[2], "*") == 0){ //Cek "*"
        min = -1;
    } else{
        printf("ERROR: Nilai menit tidak tepat\n");
        errflag = 1;
    }

    if(digits_only(argv[3])){ //Cek detik
        sec = atoi(argv[3]);
        if(sec < 0 || sec > 59){
            printf("ERROR: Nilai detik tidak tepat\n");
            errflag = 1;
        }
    } else if(strcmp(argv[3], "*") == 0){ //Cek "*"
        sec = -1;
    } else{
        printf("ERROR: Nilai detik tidak tepat\n");
        errflag = 1;
    }

    path = argv[4];
    
    if(access(path, F_OK) == 0){ //Cek path
        printf("Berhasil menemukan alamat\n");
    } else{
        printf("ERROR: Tidak berhasil menemukan script\n");
        errflag = 1;
    }
```
Selanjutnya dilakukan pengecekan input, argv[1] menyimpan nilai input jam, argv[2] menyimpan nilai input menit, argv[3] menyimpan nilai input detik, dan argv[4] menyimpan nilai input path. Untuk jam, menit, dan detik perlu dilakukan pengecekan digits_only() dan strcmp() argumen tersebut dengan \* untuk memfasilitasi input berupa integer dan \*. Input integer jam yang diterima bernilai 0-23, input integer menit yang diterima bernilai 0-59, dan input integer detik yang diterima berupa 0-59. Apabila ditemukan \* sebagai input maka nilai waktunya akan diset menjadi -1 untuk menandai bahwa input waktu tersebut bernilai \*.

Untuk input path disimpan di argv[4], setelah disimpan di path akan dilakukan pengecekan path tersebut. Untuk semua input terdapat error message yang keluar apabila terdapat kesalahan pada input tersebut. Nilai errflag juga akan diset menjadi 1 apabila terdapat kesalahan dalam input argumen, sehingga menandakan bahwa program tersebut tidak dapat dijalankan dan akan direturn sebelum membuat proses daemon.
```
    if(errflag) { //Jika error maka keluar
        printf("ERROR: Program tidak berhasil dijalankan\n");
        printf("Keluar program...\n");
        return 0;
    }
    printf("Membuat daemon...\n");    
    startdaemon();
```
Dilakukan pengecekan errflag, apabila errflag tidak bernilai 1 maka dapat dijalankan proses daemon.
```
    while(1){

        int diff = calculatesleep(hour, min, sec); //Hitung waktu sleep ke loop selanjutnya
        sleep(diff);

        pid_t pid = fork();

        if(pid == 0){ //Child process
            execl(pbash, pbash, path, NULL); //Eksekusi bash
            exit(0);
        } else if(pid > 0){ //Parent process
            int status;
            wait(&status);
            if(WIFEXITED(status)){
                printf("Child process terminated\n");
            } else{
                printf("Child process terminated abnormally\n");
            }
        } else{
            printf("ERROR: Gagal melakukan fork\n");
        }
    }

    return 0;
}
```
Dalam proses daemon program dilakukan looping while(1), kemudian program akan menghitung waktu sleep yang dibutuhkan sesuai dengan input waktu dari program dan melakukan sleep selama hasil dari fungsi calculateSleep().

Setelah sleep selesai dijalankan, maka program akan melakukan fork dan dalam child process akan dilakukan running file di path dengan menggunakan bantuan execl. Setelah selesai maka child process akan melakukan exit.
