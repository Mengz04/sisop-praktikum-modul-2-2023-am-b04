#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include <time.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>

int calculatesleep(int hour, int min, int sec){ //Fungsi mengecek waktu sleep ke loop selanjutnya
    time_t now = time(NULL);
    struct tm *current_tm = localtime(&now);
    struct tm *target_tm = current_tm;
    int choice = 0;

    if(sec < 0 && min < 0 && hour < 0){ //case 1: h*m*s*
        target_tm->tm_sec = current_tm->tm_sec + 1;
        target_tm->tm_min = current_tm->tm_min;
        target_tm->tm_hour = current_tm->tm_hour;
    } else if(sec < 0 && min < 0 && hour >= 0){ //case 2: h0m*s*
        if(current_tm->tm_hour != hour){
            target_tm->tm_sec = 0;
            target_tm->tm_min = 0;
            target_tm->tm_hour = hour;
        }else{
            target_tm->tm_sec = current_tm->tm_sec + 1;
            target_tm->tm_min = current_tm->tm_min;
            target_tm->tm_hour = current_tm->tm_hour;
            if(target_tm->tm_sec >= 60){
                target_tm->tm_sec = 0;
                target_tm->tm_min + 1;
            }
            if(target_tm->tm_min >= 60){
                target_tm->tm_min = 0;
                target_tm->tm_hour + 1;
            }
        }
    } else if(sec < 0 && min >= 0 && hour < 0){ //case 3: h*m0s* 
        target_tm->tm_min = min;
        if(current_tm->tm_min < min){ 
            target_tm->tm_sec = 0;
            target_tm->tm_hour = current_tm->tm_hour;
        } else if(current_tm->tm_min == min){ 
            target_tm->tm_sec = current_tm->tm_sec + 1;
            target_tm->tm_hour = current_tm->tm_hour;
            if(target_tm->tm_sec >= 60){
                target_tm->tm_sec = 0;
                target_tm->tm_hour + 1;
            }
        } else{ 
            target_tm->tm_sec = 0;
            target_tm->tm_hour = current_tm->tm_hour + 1;
        }
    } else if(sec < 0 && min >= 0 && hour >= 0){ //case 4: h0m0s*
        if(current_tm->tm_hour != hour || current_tm->tm_min != min){
            target_tm->tm_sec = 0;
            target_tm->tm_min = min;
            target_tm->tm_hour = hour;
        }else{
            target_tm->tm_sec = current_tm->tm_sec + 1;
            target_tm->tm_min = min;
            target_tm->tm_hour = hour;
            if(target_tm->tm_sec >= 60){
                target_tm->tm_sec = 0;
                target_tm->tm_min = min;
                target_tm->tm_hour = hour;
            }
        }
    } else if(sec >= 0 && min < 0 && hour < 0){ //case 5: h*m*s0
        if(current_tm->tm_sec < sec){
            target_tm->tm_sec = sec;
            target_tm->tm_min = current_tm->tm_min;
            target_tm->tm_hour = current_tm->tm_hour;
        } else {
            current_tm->tm_sec = sec;
            target_tm->tm_min = current_tm->tm_min + 1;
            target_tm->tm_hour = current_tm->tm_hour;
            if(target_tm->tm_min >= 60){
                target_tm->tm_hour += 1;
            }
        }
    } else if(sec >= 0 && min < 0 && hour >= 0){ //case 6: h0m*s0
        if(current_tm->tm_hour != hour){
            target_tm->tm_hour = hour;
            target_tm->tm_sec = sec;
            target_tm->tm_min = 0;
        } else{
            if(current_tm->tm_sec < sec){
                target_tm->tm_hour = hour;
                target_tm->tm_min = current_tm->tm_min;
                target_tm->tm_sec = sec;
            }else if(current_tm->tm_sec == sec){
                target_tm->tm_hour = hour;
                target_tm->tm_min = current_tm->tm_min + 1;
                target_tm->tm_sec = sec;
                if(target_tm->tm_min >= 60){
                    target_tm->tm_min = 0;
                    target_tm->tm_hour = hour;
                }
            }else{
                target_tm->tm_hour = hour;
                target_tm->tm_sec = sec;
                target_tm->tm_min = current_tm->tm_min + 1;
                if(target_tm->tm_min >= 60){
                    target_tm->tm_min = 0;
                    target_tm->tm_hour = hour;
                }
            }
        }
    } else if(sec >= 0 && min >= 0 && hour < 0){ //case 7: h*m0s0
        if(current_tm->tm_min != min || current_tm->tm_sec != sec){
            target_tm->tm_sec = sec;
            target_tm->tm_min = min;
            target_tm->tm_hour = current_tm->tm_hour;
        } else{
            target_tm->tm_sec = sec;
            target_tm->tm_min = min;
            target_tm->tm_hour = current_tm->tm_hour + 1;
            if(target_tm->tm_hour >= 23){
                target_tm->tm_sec = sec;
                target_tm->tm_min = min;
                target_tm->tm_hour = 0;
                target_tm->tm_mday = current_tm->tm_mday + 1;
            }
        }
    } else{ //case 8: h0m0s0
        target_tm->tm_sec = sec;
        target_tm->tm_min = min;
        target_tm->tm_hour = hour;
    }
    time_t target = mktime(target_tm);
    double diff = difftime(target, now);

    if (diff == 0) diff++;

    if(diff < 0){
        if(sec < 0) diff++;
        if(diff < 0 && min < 0) diff += 60;
        if(diff < 0 && hour < 0) diff += 3600;
        if(diff < 0) diff += 86400;
    } 

    printf("Tidur selama %.1lf detik\n", diff);
    return diff;
}

void startdaemon(){ //Membuat daemon process
    pid_t pid, sid;

    pid = fork();

    if(pid < 0){
        exit(EXIT_FAILURE);
    } else if(pid > 0){
        exit(EXIT_SUCCESS);
    }

    umask(0);
    sid = setsid();
    
    if(sid < 0){
        exit(EXIT_FAILURE);
    }
    
    if((chdir("/")) < 0){
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
}

int digits_only(const char *s){ //Mengecek apakah argumen berupa integer
    while(*s){
        if(isdigit(*s++) == 0) return 0;
    }
    return 1;
}

int main (int argc, char *argv[]){
    int errflag = 0;
    int hour, min, sec;
    char *path;
    char *pbash = "/bin/bash";

    if(argc != 5){
        printf("ERROR: Jumlah argumen tidak sesuai\n");
        return 0;
    }

    if(digits_only(argv[1])){ //Cek jam
        hour = atoi(argv[1]);
        if(hour < 0 || hour > 23){
            printf("ERROR: Nilai jam tidak tepat\n");
            errflag = 1;
        }
    } else if(strcmp(argv[1], "*") == 0){ //Cek "*"
        hour = -1;
    } else{
        printf("ERROR: Nilai jam tidak tepat\n");
        errflag = 1;
    }

    if(digits_only(argv[2])){ //Cek Menit
        min = atoi(argv[2]);
        if(min < 0 || min > 59){
            printf("ERROR: Nilai menit tidak tepat\n");
            errflag = 1;
        }
    } else if(strcmp(argv[2], "*") == 0){ //Cek "*"
        min = -1;
    } else{
        printf("ERROR: Nilai menit tidak tepat\n");
        errflag = 1;
    }

    if(digits_only(argv[3])){ //Cek detik
        sec = atoi(argv[3]);
        if(sec < 0 || sec > 59){
            printf("ERROR: Nilai detik tidak tepat\n");
            errflag = 1;
        }
    } else if(strcmp(argv[3], "*") == 0){ //Cek "*"
        sec = -1;
    } else{
        printf("ERROR: Nilai detik tidak tepat\n");
        errflag = 1;
    }

    path = argv[4];

    if(access(path, F_OK) == 0){ //Cek path
        printf("Berhasil menemukan alamat\n");
    } else{
        printf("ERROR: Tidak berhasil menemukan script\n");
        errflag = 1;
    }

    if(errflag) { //Jika error maka keluar
        printf("ERROR: Program tidak berhasil dijalankan\n");
        printf("Keluar program...\n");
        return 0;
    }

    printf("Membuat daemon...\n");
    
    startdaemon();

    while(1){

        int diff = calculatesleep(hour, min, sec); //Hitung waktu sleep ke loop selanjutnya
        sleep(diff);

        pid_t pid = fork();

        if(pid == 0){ //Child process
            execl(pbash, pbash, path, NULL); //Eksekusi bash
            exit(0);
        } else if(pid > 0){ //Parent process
            int status;
            wait(&status);
            if(WIFEXITED(status)){
                printf("Child process terminated\n");
            } else{
                printf("Child process terminated abnormally\n");
            }
        } else{
            printf("ERROR: Gagal melakukan fork\n");
        }
    }

    return 0;
}
