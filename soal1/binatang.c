#include<stdio.h>
#include<stdlib.h>
#include <zip.h>
#include <zlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <dirent.h>
#include <string.h>

int main(){
	pid_t pid = fork();
	int status;
	if(pid == 0){
		//download zip
                char *download[]={"wget", "-O", "coba.zip","https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq",NULL};
                execv("/usr/bin/wget", download);

	}else{
		//tunggu selesai zip
		while ((wait(&status)) > 0);
		pid_t pidm = fork();
		int statusm;
		if(pidm==0){
			//buat file untuk simpan gambar
			char *mkdi[]={"mkdir", "file", NULL};
			execv("/usr/bin/mkdir", mkdi);
		}else{
			pid_t pid2 = fork();
			int status2;
			char path[1024];
   			getcwd(path, sizeof(path));
    		strcat(path, "/file");
			if(pid2==0){
				//unzip ke file
                char *args[] = {"unzip", "coba.zip","-d", path,  NULL};
        	    execv("/usr/bin/unzip", args);

			}else{
				//tunggu selesai unzip
				while ((wait(&status2)) > 0);
				pid_t pid3 = fork();
				if(pid3==0){
					//hapus file zip yg didownload
					char *remove[]={"rm", "coba.zip", NULL};
	                		execv("/usr/bin/rm", remove);
				}else{
					pid_t pid4 = fork();
					if(pid4==0){
						//buat folder hewan darat, air, amphibi
						char *mkdir[]={"mkdir", "HewanDarat", "HewanAmphibi", "HewanAir", NULL};
						execv("/usr/bin/mkdir", mkdir);
					}else{
						pid_t pid5 = fork();
						int status3;
						if(pid5 == 0){
							//output random
							char command[200];
        					sprintf(command, "ls %s | shuf -n 1", path);
        					system("echo hewan random :");
        					system(command);
						}else{
							//tunggu ambil hewan random
							while((wait(&status3)) > 0);
							pid_t pid6 = fork();
							int status4;
							if(pid6 == 0){
								//masukin ke folder
								

								char last[200];
   								DIR *d;
  								struct dirent *dir;
							        const char *prefix1 = "_darat.jpg";
    								const char *prefix2 = "_air.jpg";
    								const char *prefix3 = "_amphibi.jpg";
    								//const char *path = "/home/vagrant/SisopSoal1/file";
    								
    								// open current directory
    								d = opendir(path);
    								if (d) {
    								    while ((dir = readdir(d)) != NULL){
    								        if (strstr(dir->d_name, prefix1) != NULL) {
    								        	char path2[1024];
   												getcwd(path2, sizeof(path2));
    											strcat(path2, "/HewanDarat");
    								            snprintf(last, sizeof(last), "mv %s/%s %s", path, dir->d_name, path2);
    								            // move file to new directory
    								            printf("%s\n\n", last);
    								            system(last);
    								        }else if(strstr(dir->d_name, prefix2) != NULL){
    								        	char path2[1024];
   												getcwd(path2, sizeof(path2));
    											strcat(path2, "/HewanAir");
    								            snprintf(last, sizeof(last), "mv %s/%s %s", path, dir->d_name, path2);
    								            // move file to new directory
    								            printf("%s\n\n", last);
    								            system(last);
   								            }else if(strstr(dir->d_name, prefix3) != NULL){
   								            	char path2[1024];
   												getcwd(path2, sizeof(path2));
    											strcat(path2, "/HewanAmphibi");
   								            	snprintf(last, sizeof(last), "mv %s/%s %s", path, dir->d_name, path2);
   								            	// move file to new directory
   								                printf("%s\n\n", last);
   								                system(last);
   								         }
   								     }
  								     closedir(d);
 								 }
								system("rmdir file");
								
							}else{
								while((wait(&status4)) > 0);
								//zip file
								pid_t pid7 = fork();
								if(pid7==0){
									char *zip1[]={"zip", "-r", "HewanDarat.zip", "HewanDarat", NULL};
									execv("/usr/bin/zip", zip1);
								}else{
									pid_t pid8 = fork();
									if(pid8==0){
										char *zip2[]={"zip", "-r", "HewanAir.zip", "HewanAir", NULL};
                                                                        	execv("/usr/bin/zip", zip2);
									}else{
										pid_t pid9 = fork();
										int status5;
										if(pid9==0){
											char *zip3[]={"zip", "-r", "HewanAmphibi.zip", "HewanAmphibi", NULL};
		                                                                        execv("/usr/bin/zip", zip3);
										}else{
											while((wait(&status5)) > 0);
											system("rm -r HewanDarat HewanAir HewanAmphibi");
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	return 0;
}
