#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <string.h>
#include <dirent.h>

void buatTim(int bek, int gelandang, int penyerang);

void recursiveList(char *basePath, FILE *fp, int num);

int main(){
   pid_t child_id;
   
   child_id = fork();
   if(child_id < 0){exit(EXIT_FAILURE);}
   
   if(child_id == 0){
   	char *download[] = {"wget", "-O", "players.zip", "https://drive.google.com/uc?export=download&id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF", NULL};
   	execv("/usr/bin/wget", download);
   	exit(EXIT_SUCCESS);
   }
   else{
   	wait(NULL);
   	pid_t child_id2;
   
   	child_id2 = fork();
   	if(child_id2 < 0){exit(EXIT_FAILURE);}
   	
   	if(child_id2 == 0){
	   pid_t child_unzip;
	   child_unzip = fork();
	   if(child_unzip < 0){exit(EXIT_FAILURE);}
   	   
   	   if(child_unzip == 0){
   	   	char *unzip[] = {"unzip", "players.zip", NULL};
   	   	execv("/usr/bin/unzip", unzip);
   	   	exit(EXIT_SUCCESS);
	   }
	   else{
		wait(NULL);
		execlp("rm", "rm", "players.zip", (char *) NULL);
	   }
	   exit(EXIT_SUCCESS);
   	}
   	else{
   	   wait(NULL);

   	   pid_t child_id3;
   	   
   	   child_id3 = fork();
   	   if(child_id3 < 0){exit(EXIT_FAILURE);}
   	   
   	   if(child_id3 == 0){
   	   	char *findMU[] = {"find", "./players", "-type", "f", "!", "-name", "*_ManUtd*", "-delete", NULL};
   	   	execvp(findMU[0], findMU);
   	   	exit(EXIT_SUCCESS);
   	   }
   	   else{
   	   	wait(NULL);
   	   	pid_t child_id4;
   	   	
   	   	child_id4 = fork();
   	   	if(child_id4 < 0){exit(EXIT_FAILURE);}
   	   
   	   	if(child_id4 == 0){
   	   		wait(NULL);
	   	   	pid_t child_id5;
	   	   	
	   	   	child_id5 = fork();
	   	   	if(child_id5 < 0){exit(EXIT_FAILURE);}
	   	   
	   	   	if(child_id5 == 0){
	   	   	   pid_t child_Dir1;
	   	   	   child_Dir1 = fork();
	   	   	   
	   	   	   if(child_Dir1 < 0){exit(EXIT_FAILURE);}
	   	   	   
	   	   	   if(child_Dir1 == 0){
	   	   	   	char *GKCategory[] = {"mkdir", "-p", "./players/Kiper", NULL};
	   	   		execv("/bin/mkdir", GKCategory);
	   	   		exit(EXIT_SUCCESS);
	   	   	   }
	   	   	   else{
	   	   	   	wait(NULL);
	   	   	   	pid_t child_Dir2;
	   	   	   	child_Dir2 = fork();
	   	   	   
	   	   	   	if(child_Dir2 < 0){exit(EXIT_FAILURE);}
	   	   	   
	   	   	   	if(child_Dir2 == 0){
	   	   	   	   char *BKCategory[] = {"mkdir", "-p", "./players/Bek", NULL};
	   	   		   execv("/bin/mkdir", BKCategory);
	   	   		   exit(EXIT_SUCCESS);
	   	   	   	}
	   	   	   	else{
	   	   	   	   wait(NULL);
	   	   	   	   pid_t child_Dir3;
	   	   	   	   child_Dir3 = fork();
	   	   	   
	   	   	   	   if(child_Dir3 < 0){exit(EXIT_FAILURE);}
	   	   	   
	   	   	   	   if(child_Dir3 == 0){
	   	   	   	   	char *GLCategory[] = {"mkdir", "-p", "./players/Gelandang", NULL};
	   	   			execv("/bin/mkdir", GLCategory);
	   	   			exit(EXIT_SUCCESS);
	   	   	   	   }
	   	   	   	   else{
	   	   	   	   	wait(NULL);
	   	   	   	   	char *STCategory[] = {"mkdir", "-p", "./players/Penyerang", NULL};
	   	   			execv("/bin/mkdir", STCategory);
	   	   	   	   }
	   	   	   	}
	   	   	   }
	   	   	exit(EXIT_SUCCESS);
	   	   	}
	   	   	else{
	   	   	  wait(NULL);
			  pid_t child_cp;
			  
			  child_cp = fork();
			  
			  if(child_cp < 0){exit(EXIT_FAILURE);}
			  
			  if(child_cp == 0){
			  	pid_t child_subcp1;
			  	child_subcp1 = fork();
			  	
			  	if(child_subcp1 < 0){exit(EXIT_FAILURE);}
			  
			  	if(child_subcp1 == 0){
			  		char *copyGK[] = {"find", "./players","-type", "f", "-name", "*_*_Kiper*", "-exec", "mv", "-n", "{}", "./players/Kiper", ";", NULL};
	   	   	   		execvp("find", copyGK);
	   	   	   		exit(EXIT_SUCCESS);
			  	}
			  	else{
			  		char *copyBK[] = {"find", "./players", "-type", "f", "-name", "*_*_Bek*", "-exec", "mv", "-n", "{}", "./players/Bek", ";", NULL};
	   	   	   		execvp("find", copyBK);
			  	}
			  }
			  else{
			  	pid_t child_subcp2;
			  	child_subcp2 = fork();
			  	
			  	if(child_subcp2 < 0){exit(EXIT_FAILURE);}
			  
			  	if(child_subcp2 == 0){
			  		char *copyGL[] = {"find", "./players", "-type", "f", "-name", "*_*_Gelandang*", "-exec", "mv", "-n", "{}", "./players/Gelandang", ";", NULL};
	   	   	   		execvp("find", copyGL);
			  	}
			  	else{
			  		char *copyST[] = {"find", "./players", "-type", "f", "-name", "*_*_Penyerang*", "-exec", "mv","-n", "{}", "./players/Penyerang", ";", NULL};
	   	   	   		execvp("find", copyST);
			  	}
			  }
	   	   	}
   	   		exit(EXIT_SUCCESS);
   	   	}
   	   	else{
 			wait(NULL);
 			pid_t child_txt;
 			
			child_txt = fork();
			  	
			if(child_txt < 0){exit(EXIT_FAILURE);}
			
			if(child_txt == 0){ //child
				char file_path[50];
				sprintf(file_path, "%s/Formasi-3-3-4.txt", getenv("HOME"));
				char *touchTxt[] = {"touch", file_path, NULL};
				execvp("touch", touchTxt);
				exit(EXIT_SUCCESS);
			}
			else{
				wait(NULL);
				buatTim(3, 3, 4);
	 		}
   	   	}
   	 }
    }
  }
}

void buatTim(int bek, int gelandang, int penyerang){
	char file_dest[50];
	FILE *fp;
	
	sprintf(file_dest, "%s/Formasi-3-3-4.txt", getenv("HOME"));
	fp = fopen(file_dest, "w");

	recursiveList("players/Kiper", fp, 1);
	recursiveList("players/Bek", fp, bek);
	recursiveList("players/Gelandang", fp, gelandang);
	recursiveList("players/Penyerang", fp, penyerang);
}

void recursiveList(char *basePath, FILE *fp, int num){		
	DIR *dp;
    struct dirent *rate, *max;
    
    char highestStr[3];
    char visited[num][100];
    
    int visitCount=0;
	int highest=0;

	while(num--){
		int highest=0;
		dp = opendir(basePath);
		if(dp == NULL){exit(EXIT_FAILURE);}
		while ((rate = readdir(dp))!= NULL) {
			if(!strcmp(rate->d_name, ".") || !strcmp(rate->d_name, "..")){continue;}
			int flag = 0;
			for(int i=0; i<visitCount; i++){
				if(!strcmp(visited[i], rate->d_name)){flag = 1;}
			}
			if(flag == 1){continue;}

			char *scoreStr = strrchr(rate->d_name, '_');
			int score = atoi(scoreStr+1);
			highest = (score > highest)? score : highest;
		}

		sprintf(highestStr, "%d", highest);
		dp = opendir(basePath);
		
		while ((max = readdir(dp)) != NULL) {
			if(!strcmp(max->d_name, ".") || !strcmp(max->d_name, "..")){continue;}
			if(strstr(max->d_name, highestStr) != NULL){
				fprintf(fp, "%s\n", max->d_name);
				strcpy(visited[visitCount], max->d_name);
				visitCount++;
			}
		}
	}
	(void) closedir (dp);
}
